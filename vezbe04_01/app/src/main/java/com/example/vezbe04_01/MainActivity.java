package com.example.vezbe04_01;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int UI_ANIMATION_DELAY = 300;
    private View relativeLayout;
    private final Handler hideHandler = new Handler();

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            relativeLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        relativeLayout = findViewById(R.id.aMRelativeLayout);
        this.hide();

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout aMLinearLayoutVertical = (LinearLayout) findViewById(R.id.aMLinearLayoutVertical);
        ArrayList<Kontakt> kontakti = KontaktApi.getKontakti();

        ScrollView parentScrollView = (ScrollView) findViewById(R.id.scrollView1);


        for(Kontakt kontakt:kontakti){
            RelativeLayout relativeLayoutMyView = (RelativeLayout) inflater.inflate(R.layout.my_view, null);

            ScrollView childScrollView = (ScrollView) relativeLayoutMyView.findViewById(R.id.scrollView2);

//            parentScrollView.setOnTouchListener(new View.OnTouchListener() {
//                public boolean onTouch(View p_v, MotionEvent p_event) {
//                    childScrollView.getParent().requestDisallowInterceptTouchEvent(
//                            false);
//                    // We will have to follow above for all scrollable contents
//                    return false;
//                }
//            });

//            childScrollView.setOnTouchListener(new View.OnTouchListener() {
//                public boolean onTouch(View p_v, MotionEvent p_event) {
//                    // this will disallow the touch request for parent scroll on
//                    // touch of child view
//                    findViewById(R.id.scrollView1).getParent().requestDisallowInterceptTouchEvent(true);
////                    p_v.getParent().requestDisallowInterceptTouchEvent(true);
//                    return false;
//                }
//            });

            ((TextView) relativeLayoutMyView.findViewById(R.id.mVTextViewIme)).setText(kontakt.getIme());
            ((TextView) relativeLayoutMyView.findViewById(R.id.mVTextViewPrezime)).setText(kontakt.getPrezime());
            ((TextView) relativeLayoutMyView.findViewById(R.id.mVTextViewPrica)).setText("Poruka: " + kontakt.getPrica());

//            -------------------------------------------------------------------------------------------------
            LinearLayout mVLinearLayoutVertical = (LinearLayout) relativeLayoutMyView.findViewById(R.id.mVLinearLayoutVertical);

            HashMap<Kontakt.TIP_KONTAKTA, String> recnikVrednostiKontakta = kontakt.getRecnikVrednostiKontakta();
            boolean phone = false;
            boolean email = false;
            boolean skype = false;
            for(int i = 0; i < recnikVrednostiKontakta.size(); i++){
                RelativeLayout relativeLayoutMyView2 = (RelativeLayout) inflater.inflate(R.layout.my_view2, null);

                ImageView mV2ImageView = (ImageView) relativeLayoutMyView2.findViewById(R.id.mV2ImageView);

                if(recnikVrednostiKontakta.get(Kontakt.TIP_KONTAKTA.PHONE) != null && !phone){
                    mV2ImageView.setImageResource(R.drawable.phone);
                    ((TextView) relativeLayoutMyView2.findViewById(R.id.mV2TextViewKontakt)).setText(recnikVrednostiKontakta.get(Kontakt.TIP_KONTAKTA.PHONE));
                    phone = true;
                } else if(recnikVrednostiKontakta.get(Kontakt.TIP_KONTAKTA.EMAIL) != null && !email){
                    mV2ImageView.setImageResource(R.drawable.email);
                    ((TextView) relativeLayoutMyView2.findViewById(R.id.mV2TextViewKontakt)).setText(recnikVrednostiKontakta.get(Kontakt.TIP_KONTAKTA.EMAIL));
                    email = true;
                } else if(recnikVrednostiKontakta.get(Kontakt.TIP_KONTAKTA.SKYPE) != null && !skype){
                    mV2ImageView.setImageResource(R.drawable.skype);
                    ((TextView) relativeLayoutMyView2.findViewById(R.id.mV2TextViewKontakt)).setText(recnikVrednostiKontakta.get(Kontakt.TIP_KONTAKTA.SKYPE));
                    skype = true;
                }

                mVLinearLayoutVertical.addView(relativeLayoutMyView2);
            }
//            -------------------------------------------------------------------------------------------------

            aMLinearLayoutVertical.addView(relativeLayoutMyView);
        }

        ((Button) findViewById(R.id.aMButton)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    private void hide() {
//        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        hideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }
}
