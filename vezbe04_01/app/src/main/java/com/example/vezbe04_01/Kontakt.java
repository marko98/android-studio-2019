package com.example.vezbe04_01;

import java.util.ArrayList;
import java.util.HashMap;

public class Kontakt {
    public enum TIP_KONTAKTA {EMAIL, PHONE, SKYPE};

    private String ime, prezime, prica;
    private HashMap<TIP_KONTAKTA, String> recnikVrednostiKontakta;

//    alt+insert za automatsko generisanje stvari

    public Kontakt(String ime, String prezime, HashMap<TIP_KONTAKTA, String> recnikVrednostiKontakta, String prica) {
        this.ime = ime;
        this.prezime = prezime;
        this.recnikVrednostiKontakta = recnikVrednostiKontakta;
        this.prica = prica;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public HashMap<TIP_KONTAKTA, String> getRecnikVrednostiKontakta() {
        return recnikVrednostiKontakta;
    }

    public String getPrica() {
        return prica;
    }
}
