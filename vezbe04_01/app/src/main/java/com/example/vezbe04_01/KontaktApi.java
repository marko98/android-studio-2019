package com.example.vezbe04_01;

import java.util.ArrayList;
import java.util.HashMap;

public class KontaktApi {
    public static ArrayList<Kontakt> getKontakti(){
        ArrayList<Kontakt> kontakti = new ArrayList<Kontakt>();

//        ArrayList<HashMap<Kontakt.TIP_KONTAKTA, String> > listaSima = new ArrayList<HashMap<Kontakt.TIP_KONTAKTA, String> >();
        HashMap<Kontakt.TIP_KONTAKTA, String> mapSima = new HashMap<Kontakt.TIP_KONTAKTA, String>();
        mapSima.put(Kontakt.TIP_KONTAKTA.PHONE, "+381644923082");
        mapSima.put(Kontakt.TIP_KONTAKTA.SKYPE, "sima.skype.98");
//        listaSima.add(mapSima);
        kontakti.add(new Kontakt("Sima", "Lagundzin", mapSima, "Programer sam u dusi :)"));

//        ArrayList<HashMap<Kontakt.TIP_KONTAKTA, String> > listaMarko = new ArrayList<HashMap<Kontakt.TIP_KONTAKTA, String> >();
        HashMap<Kontakt.TIP_KONTAKTA, String> mapMarko = new HashMap<Kontakt.TIP_KONTAKTA, String>();
        mapMarko.put(Kontakt.TIP_KONTAKTA.SKYPE, "marko.skype.98");
//        listaMarko.add(mapMarko);
        kontakti.add(new Kontakt("Marko", "Zahorodni", mapMarko, "Volim Malisu <3"));

//        ArrayList<HashMap<Kontakt.TIP_KONTAKTA, String> > listaDavid = new ArrayList<HashMap<Kontakt.TIP_KONTAKTA, String> >();
        HashMap<Kontakt.TIP_KONTAKTA, String> mapDavid = new HashMap<Kontakt.TIP_KONTAKTA, String>();
        mapDavid.put(Kontakt.TIP_KONTAKTA.EMAIL, "david@gmail.com");
//        listaDavid.add(mapDavid);
        kontakti.add(new Kontakt("David", "Zahorodni", mapDavid, "Volim Marka"));

        return kontakti;
    }
}
