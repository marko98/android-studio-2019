package com.example.vezbe10;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
//    ZA SHARED PREFERENCES --------------------------------------------------------------------------
//    SLUZI VISE ZA CUVANJE PRIMITIVNIH PODATAKA KAO STRING, BOOLEAN, INT itd...

//    private final static String SHARED_PREFERENCES_PREFIX = "MainActivitySharedPreferencesPrefix";
//    private final static String SHARED_PREFERENCES_IME_PREZIME = "ime_prezime";
//    private final static String SHARED_PREFERENCES_EMAIL = "email_address";
//    -------------------------------------------------------------------------------------------------

    //    ZA INTERNAL STORAGE -----------------------------------------------------------------------------
//    private final static String APP_DATA_PREFIX = "MainActivityDataFile";
    //    -------------------------------------------------------------------------------------------------

    //    ZA EXTERNAL STORAGE -----------------------------------------------------------------------------
//    kreirani folderi i fajlovi su dostupni korisnicima!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    private final static String APP_DATA_PREFIX = "MainActivityDataDirectory";
    private final static String APP_DATA_FILE = "/podaci-korisnika.txt";
    //    -------------------------------------------------------------------------------------------------

    private EditText aMEditTextImeIPrezime;
    private EditText aMEditTextEmail;
    private Button aMButtonSacuvaj;
    private Button aMButtonProcitaj;
    private TextView aMTextViewTekst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
    }

    private void initComponents(){
        aMEditTextImeIPrezime = (EditText) findViewById(R.id.aMEditTextImeIPrezime);
        aMEditTextEmail = (EditText) findViewById(R.id.aMEditTextEmail);
        aMButtonSacuvaj = (Button) findViewById(R.id.aMButtonSacuvaj);
        aMButtonProcitaj = (Button) findViewById(R.id.aMButtonProcitaj);
        aMTextViewTekst = (TextView) findViewById(R.id.aMTextViewTekst);

        aMButtonSacuvaj.setOnClickListener(this);
        aMButtonProcitaj.setOnClickListener(this);
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//
//        sacuvajPodatke();
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.aMButtonSacuvaj:
                sacuvajPodatke();
                break;
            case R.id.aMButtonProcitaj:
                procitajPodatke();
                break;
        }
    }

    private boolean isExternalStorageWritable(){
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            Log.i("State", "Yes, it is writable!");
//            Toast.makeText(this, "Yes, it is writable!", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

    private boolean checkPermission(String permission){
        int check = ContextCompat.checkSelfPermission(this, permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

//    @TargetApi(23)
    @RequiresApi(23)
    private void requestPermissionFromUser(){
//        u ozbiljnijim aplikacijama fali jos deo da se proveri da li je korisnik dozvolio upis!!!
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        requestPermissions(permissions, 1);
    }

    private void sacuvajPodatke(){
        String imeIPrezime = aMEditTextImeIPrezime.getText().toString();
        String email = aMEditTextEmail.getText().toString();

        //    ZA SHARED PREFERENCES --------------------------------------------------------------------------
//        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_PREFIX, 0);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString(SHARED_PREFERENCES_IME_PREZIME, imeIPrezime);
//        editor.putString(SHARED_PREFERENCES_EMAIL, email);
//        editor.commit();
        //    -------------------------------------------------------------------------------------------------

        //    ZA INTERNAL STORAGE -----------------------------------------------------------------------------
////        izvrsava se samo iz ovog thread-a(niti)
//
//        try {
////          APP_DATA_PREFIX je ime fajla, a Context.MODE_PRIVATE znaci da pravimo privatan fajl dostupan samo nasoj aplikaciji
//            FileOutputStream fos = openFileOutput(APP_DATA_PREFIX, Context.MODE_PRIVATE);
//            PrintWriter pw = new PrintWriter(fos);
//            pw.println(imeIPrezime);
//            pw.println(email);
//            pw.flush();
//            pw.close();
//            fos.close();
//        } catch (Exception e) {
////
//        };
        //    -------------------------------------------------------------------------------------------------

        //    ZA EXTERNAL STORAGE -----------------------------------------------------------------------------

//        kreiramo FOLDER(DIREKTORIJUM) ciju putanju za externu karticu dobijamo iz Environment.getExternalStoragePublicDirectory, zatim kazemo da zelimo npr. da koristimo DOCUMENTS folder(Environment.DIRECTORY_DOCUMENTS),
//        kao drugi argument se daje naziv FOLDER-a(DIREKTORIJUM-a) koji ce se unutar direktorijuma za dokumente kreirati
//        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), APP_DATA_PREFIX);
////        ukoliko taj FOLDER(DIREKTORIJUM) ne postoji on ce ga napraviti a ako postoji nista se nece desiti
//        dir.mkdirs();
//
////        kreiramo bas FAJL za upis i citanje podataka
//        File fajl = new File(dir.getAbsolutePath() + APP_DATA_FILE);
//        try {
//            PrintWriter pw = new PrintWriter(fajl);
//            pw.println(imeIPrezime);
//            pw.println(email);
//            pw.flush();
//            pw.close();
//        } catch (Exception e){
//
//        }
        //    -------------------------------------------------------------------------------------------------

        //    ZA EXTERNAL STORAGE 2. NACIN (ne zahteva API 19)---------------------------------------------------------------------
//        if(isExternalStorageWritable() && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)){

        if(Build.VERSION.SDK_INT >= 23){
            requestPermissionFromUser();
        }

        if(isExternalStorageWritable()){
                File textFile = new File(Environment.getExternalStorageDirectory(), "imeFajla.txt");
                try{
                    PrintWriter pw = new PrintWriter(textFile);
                    pw.println(imeIPrezime);
                    pw.println(email);
                    pw.flush();
                    pw.close();

                    Toast.makeText(this, "File saved.", Toast.LENGTH_SHORT).show();
                } catch (Exception e){
                    e.printStackTrace();
//                    Log.i("State", Log.getStackTraceString(e));
//                    aMTextViewTekst.setText(Log.getStackTraceString(e));
                }
            } else {
                Toast.makeText(this, "Cannot write to external storage.", Toast.LENGTH_SHORT).show();
            }
        //    -------------------------------------------------------------------------------------------------

    }


    private void procitajPodatke(){
        //    ZA SHARED PREFERENCES --------------------------------------------------------------------------
//        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_PREFIX, 0);
//        String imeIPrezime = sharedPreferences.getString(SHARED_PREFERENCES_IME_PREZIME, "");
//        String email = sharedPreferences.getString(SHARED_PREFERENCES_EMAIL, "");

//        aMEditTextImeIPrezime.setText(imeIPrezime);
//        aMEditTextEmail.setText(email);
        //    -------------------------------------------------------------------------------------------------

        //    ZA INTERNAL STORAGE -----------------------------------------------------------------------------
//        try {
//            FileInputStream fis = openFileInput(APP_DATA_PREFIX);
//            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
//
//            String imeIPrezime = br.readLine();
//            String email = br.readLine();
//
//            aMEditTextImeIPrezime.setText(imeIPrezime);
//            aMEditTextEmail.setText(email);
//
//            br.close();
//            fis.close();
//        } catch (Exception e) {
////
//        };
        //    -------------------------------------------------------------------------------------------------

        //    ZA EXTERNAL STORAGE -----------------------------------------------------------------------------
//        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), APP_DATA_PREFIX);
//        File fajl = new File(dir.getAbsolutePath() + APP_DATA_FILE);
//        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fajl)));
//
//            String imeIPrezime = br.readLine();
//            String email = br.readLine();
//
//            aMEditTextImeIPrezime.setText(imeIPrezime);
//            aMEditTextEmail.setText(email);
//
//            br.close();
//        } catch (Exception e){
////
//        }
        //    -------------------------------------------------------------------------------------------------

        //    ZA EXTERNAL STORAGE 2. NACIN (ne zahteva API 19)---------------------------------------------------------------------
        File textFile = new File(Environment.getExternalStorageDirectory(), "imeFajla.txt");
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(textFile)));

            String imeIPrezime = br.readLine();
            String email = br.readLine();

            aMEditTextImeIPrezime.setText(imeIPrezime);
            aMEditTextEmail.setText(email);

            br.close();
        } catch (Exception e){
//
        }
        //    -------------------------------------------------------------------------------------------------

    }

}
