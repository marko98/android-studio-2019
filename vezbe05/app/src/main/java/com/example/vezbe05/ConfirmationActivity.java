package com.example.vezbe05;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        Bundle extra = getIntent().getExtras();

        String username = extra.getString("username");
        String password = extra.getString("password");
        String faculty = extra.getString("faculty");
        Boolean employed = extra.getBoolean("employed");
        String birthday = extra.getString("birthday");

        String message = "Is this information correct:\n";
        message += "Username:   " + username + "\n";
        message += "Password:   " + password + "\n";
        message += "Faculty:   " + faculty + "\n";
        message += "Birthday:   " + birthday + "\n";
        message += (employed)?"You are employed!\n":"You are not employed!\n";
        ((TextView) findViewById(R.id.aCTextViewMessage)).setText(message);
    }
}
