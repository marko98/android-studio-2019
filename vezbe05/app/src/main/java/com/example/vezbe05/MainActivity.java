package com.example.vezbe05;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();

        findViewById(R.id.aMButtonSend).setOnClickListener(this);
    }

    public void initComponents(){
        Spinner aMSpinnerFaculty = (Spinner) findViewById(R.id.aMSpinnerFaculty);

        ArrayList<String> facultyList = new ArrayList<String>();
        facultyList.add("Fakultet za racunarstvo i informatiku");
        facultyList.add("Fakultet za turisticki i hotelijerski menadzment");
        facultyList.add("Tehnicki fakultet");
        facultyList.add("Poslovni fakultet u Beogradu");

//        spiner prikazuje view-e stoga ove stringove moramo nekim npr. layer-om pretvoriti u view
//        u zagradi se nalaze argumenti koje on uzima(ArrayAdapter), prvi je context (ovaj nas context(this)), drugi je resurs layout(postoje vec ugradjeni(predefinisani) u okviru android studia),
//        treci je lista sa podacima
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, facultyList);
//        dakle ovaj adapter konvertuje stringove u view-ove
        aMSpinnerFaculty.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.aMButtonSend){
            Intent i = new Intent(this, ConfirmationActivity.class);


            String username = ((EditText) findViewById(R.id.aMEditTextUsername)).getText().toString();
            String password = ((EditText) findViewById(R.id.aMEditTextPassword)).getText().toString();

            Spinner aMSpinnerFaculty = (Spinner) findViewById(R.id.aMSpinnerFaculty);
//            aMSpinnerFaculty.getSelectedItem() vraca objekat
//            (String) aMSpinnerFaculty.getSelectedItem() mozemo castovati u String jer smo imali adapter koji iz stringova pravi view-ove
            String faculty = (String) aMSpinnerFaculty.getSelectedItem();
//            moze i ovako String faculty = (String) ((Spinner) findViewById(R.id.aMSpinnerFaculty)).getSelectedItem(); u jednoj liniji koda

            Switch aMSwitchEmployed = (Switch) findViewById(R.id.aMSwitchEmployed);
            Boolean employed = aMSwitchEmployed.isChecked();
//            Boolean employed = ((Switch) findViewById(R.id.aMSwitchEmployed)).isChecked();

            DatePicker aMDatePickerBirthday = (DatePicker) findViewById(R.id.aMDatePickerBirthday); // referenca ka date picker-u
            Calendar kalendar = Calendar.getInstance(); // dobijamo instancu kalendara, praznog
            kalendar.set(aMDatePickerBirthday.getYear(), aMDatePickerBirthday.getMonth(), aMDatePickerBirthday.getDayOfMonth());
            Date birthday = kalendar.getTime();


            Bundle extra = new Bundle();
            extra.putString("username", username);
            extra.putString("password", password);
            extra.putString("faculty", faculty);
            extra.putBoolean("employed", employed);
//            extra.putString("birthday", birthday.toString()); !!! OPASNO jer ne mozemo da znamo da li ce konverzija uvek da uspe, da bude ispravna

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String birthdayToString = sdf.format(birthday);

//            Moze i ovako -> 4 cifre koje su integeri, 2 cifre koje su integeri, 2 cifre koje su integeri
            String birthdayToString2 = String.format("%4d-%2d-%2d", aMDatePickerBirthday.getYear(), aMDatePickerBirthday.getMonth(), aMDatePickerBirthday.getDayOfMonth());

            extra.putString("birthday", birthdayToString);

            i.putExtras(extra);
            startActivity(i);
        };
    }
}
