package com.example.vezbe04;

import java.util.ArrayList;

public class KontaktApi {
    public static ArrayList<Kontakt> getMyContacts(){
        ArrayList<Kontakt> kontakti = new ArrayList<Kontakt>();

        kontakti.add(new Kontakt("Sima", "Lagundzin", "+381644923082", Kontakt.TIP_KONTAKTA.PHONE));
        kontakti.add(new Kontakt("Marko", "Zahorodni", "marko.skype.98", Kontakt.TIP_KONTAKTA.SKYPE));
        kontakti.add(new Kontakt("David", "Zahorodni", "david@gmail.com", Kontakt.TIP_KONTAKTA.EMAIL));

        kontakti.add(new Kontakt("Dalibor", "Tot", "+381644923100", Kontakt.TIP_KONTAKTA.PHONE));
        kontakti.add(new Kontakt("Milica", "Tot", "milica.skype.2000", Kontakt.TIP_KONTAKTA.SKYPE));
        kontakti.add(new Kontakt("Mina", "Gusman", "mina@gmail.com", Kontakt.TIP_KONTAKTA.EMAIL));

        kontakti.add(new Kontakt("Maja", "Zahorodni", "+381654922456", Kontakt.TIP_KONTAKTA.PHONE));
        kontakti.add(new Kontakt("Avram", "Nikolic", "avram.skype.2016", Kontakt.TIP_KONTAKTA.SKYPE));
        kontakti.add(new Kontakt("Vesna", "Zahorodni", "vesna@gmail.com", Kontakt.TIP_KONTAKTA.EMAIL));

        return kontakti;
    }
}
