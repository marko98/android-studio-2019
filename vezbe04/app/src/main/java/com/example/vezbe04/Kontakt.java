package com.example.vezbe04;

public class Kontakt {
    public enum TIP_KONTAKTA {EMAIL, PHONE, SKYPE};

    private String ime, prezime, vrednost;
    private TIP_KONTAKTA tipKontakta;

//    alt+insert za automatsko generisanje stvari

    public Kontakt(String ime, String prezime, String vrednost, TIP_KONTAKTA tipKontakta) {
        this.ime = ime;
        this.prezime = prezime;
        this.vrednost = vrednost;
        this.tipKontakta = tipKontakta;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getVrednost() {
        return vrednost;
    }

    public TIP_KONTAKTA getTipKontakta() {
        return tipKontakta;
    }
}
