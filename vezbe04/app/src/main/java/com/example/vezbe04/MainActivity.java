package com.example.vezbe04;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.mAButtonQuit).setOnClickListener(this);
        findViewById(R.id.mAButtonChangeActivity).setOnClickListener(this);

        LinearLayout LinearLayout = (LinearLayout) findViewById(R.id.mALinearLayoutScroll);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ArrayList<Kontakt> kontakti = KontaktApi.getMyContacts();
        for(Kontakt kontakt:kontakti){
            RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.my_view, null);

            ((TextView) relativeLayout.findViewById(R.id.mVTextViewIme)).setText(kontakt.getIme());
            ((TextView) relativeLayout.findViewById(R.id.mVTextViewPrezime)).setText(kontakt.getPrezime());

            ImageView mVImageView = (ImageView) relativeLayout.findViewById(R.id.mVImageViewKontaktSlika);

            switch (kontakt.getTipKontakta())
            {
                case EMAIL:
                    mVImageView.setImageResource(R.drawable.email);
                    break;
                case PHONE:
                    mVImageView.setImageResource(R.drawable.phone);
                    break;
                case SKYPE:
                    mVImageView.setImageResource(R.drawable.skype);
                    break;
            }

            ((TextView) relativeLayout.findViewById(R.id.mVTextViewKontakt)).setText(kontakt.getVrednost());

            LinearLayout.addView(relativeLayout);
        }
    }

    @Override
    public void onClick(View v){
        switch(v.getId())
        {
            case R.id.mAButtonQuit:
                this.doQuit();
                break;
            case R.id.mAButtonChangeActivity:
                this.doChangeActivity();
                break;
        }
    }

    private void doChangeActivity(){
        Intent i = new Intent(this, changedActivity.class);
        startActivity(i);
    }

    private void doQuit(){
        finish();
    }

}
